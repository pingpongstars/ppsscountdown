(function ( $ ) {
    $.fn.pingpongdown = function( options ) {

        function drawSpanSeconds(container,seconds){
            container.html("<span>"+seconds+"</span>");
        }
        var secondsInADay = 3600*24;

        function drawSpanDHMSsmall(container,seconds){
            drawSpanDHMS(container,seconds);
        }


        function drawSpanDHMS(container,seconds){
            var dd = parseInt(seconds / secondsInADay);
            seconds = seconds % secondsInADay;
            var hh = parseInt(seconds / 3600);
            var mm = parseInt( (seconds%3600) / 60);
            var ss = seconds % 60;
            //console.log(hh,mm,ss);
            container.html('<span class="ppssclock"><span class="days">'+
                (dd<100?("00"+dd).slice(-2):dd.toString()) +'</span><span class="colon">:</span><span class="hours">'+
                ("00"+hh).slice(-2) +'</span><span class="colon">:</span><span class="minutes">'+
                ("00"+mm).slice(-2) +'</span><span class="colon">:</span><span class="seconds">'+
                ("00"+ss).slice(-2)
                +"</span></span>");
        }
        function drawSpanDHM(container,seconds){
        seconds += 60;
            var dd = parseInt(seconds / secondsInADay);
            seconds = seconds % secondsInADay;
            var hh = parseInt(seconds / 3600);
            var mm = parseInt( (seconds%3600) / 60);
            var ss = seconds % 60;
            //console.log(hh,mm,ss);
            container.html("<span class='days'>"+
                (dd<100?("00"+dd).slice(-2):dd) +"</span>&nbsp;:&nbsp;<span class='hour'>"+
                ("00"+hh).slice(-2) +"</span>&nbsp;:&nbsp;<span class='minutes'>"+
                ("00"+mm).slice(-2)
                +"</span>");
        }
        function getDistanceFromDate(start,end){
            return parseInt((end - start)/1000);
        }


        function countdown_pl(me, options){
            var cbme = this;


            this.draw = function(){
                //drawSpanSeconds(this.me,this.seconds2event);
                //drawSpanHMS(this.me,this.seconds2event);
                this.drawfunc(this.me,this.seconds2event)
            };
            
            this.getCurrentNow = function(){
            var desturl = document.location.pathname + "?ppsscd="+(new Date()).getTime();
            $.ajax(document.location.desturl,
                {complete:function(res,text){
                        //console.log(res);
                        //console.log(text);
                        //cbme.settings.endtimedelta = getDistanceFromDate(today,cbme.settings.enddate);
                        console.log("date read from server:", res.getResponseHeader("Date") );
                        var actnow = new Date(res.getResponseHeader("Date"));
                        if (actnow < ( new Date(2000,1,1,0,0,0) ) ){  //IE: i got a fake date
                            actnow = new Date();
                        }
                        cbme.settings.endtimedelta = getDistanceFromDate(actnow ,cbme.settings.enddate);
                        cbme.canstart = true;
                        cbme.try2start();
                    }

                }
            );

            };
            this.mainLoop = function(me){
                me.seconds2event = me.settings.endtimedelta - me.drawn;
                me.drawn++;
        if (me.seconds2event<=0){
            clearInterval(me.timer);
            me.timer=undefined;
            console.log("the time is over");
            me.settings.onEndCB(me);
        }
                me.draw();
            };

            this.try2start=function(){
                if (this.canstart){
            if (this.settings.endtimedelta<=0){
            this.settings.onPastEndCB(this);
            return;
            }
                    if (this.timer!==undefined){
                        clearInterval(this.timer);
                    }
            this.settings.onStart(this);
                    this.timer = setInterval(this.mainLoop.bind(null,this),1000);
                }
            }

	    //var me = this;
            this.settings = $.extend({
                drawfunc:'seconds',
                endtimedelta:36000,
                enddate:undefined,
                onPastEndCB: function(me){me.me.trigger("ppsscountdowninpast");},
                onEndCB:function(me){me.me.trigger("ppsscountdownend");},
                onStart:function(me){me.me.trigger("ppsscountdownstart");}
            },options);

            this.canstart = true;
            if (this.settings.enddate !== undefined ){
                var today = new Date();
                this.settings.endtimedelta = getDistanceFromDate(today,this.settings.enddate);
                this.canstart = false;
                this.getCurrentNow();
            }else{
                var today = new Date();
                this.settings.enddate = new Date(today.getTime() + this.settings.endtimedelta*1000);
            }
            
            this.me = me;
            this.start = new Date();
            this.drawn = 0;
            this.seconds2event = this.settings.endtimedelta - this.drawn;
            if (this.settings.drawfunc == 'seconds'){
                this.drawfunc = drawSpanSeconds;
            }else if (this.settings.drawfunc == 'DDHHMMSS'){
                this.drawfunc = drawSpanDHMS;
            }else if (this.settings.drawfunc == 'DDHHMMSSsmall'){
                this.drawfunc = drawSpanDHMSsmall;
            }else if (this.settings.drawfunc == 'DDHHMM'){
                this.drawfunc = drawSpanDHM;
            }
            this.try2start()
            return this;
        }

        //init

        var me = $(this);
        var pl = new countdown_pl(me,options);
        return pl;

    }
}( jQuery ));
