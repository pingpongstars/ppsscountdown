Javascript plugin for jQuery (requires jQuery 1.12+).


sample (and simple) usage:

* download the javascript or clone the repository. Move the script to some location ( ie: WEBROOT/path/to/js/ppsscountdown.jq.js )

* include the plugin anywhere in your page __after__ the jQuery inclusion, as such:
```HTML
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="ppsscountdown.jq.js" type="text/javascript"></script>
```

* Create a container in page to host the count down:
```HTML
<div id="countdownbox"></div>
```

* Start the counter to a given date for example December, the 31st, 2020:
```HTML
<script type="text/javascript">
      $("#clockbox").pingpongdown({drawfunc:'DDHHMMSS',enddate:new Date(2020,12-1,31,0,0,0)} );
</script>
```
 
